import React, { useEffect, useState } from 'react';
import { LinearProgress, TextField } from '@mui/material';
import Axios from 'axios';
import moment from 'moment';
import './App.css';

function App() {
    const [city, setCity] = useState('');
    const [dataCurrent, setDataCurrent] = useState([]);
    const [dataForecast, setDataForecast] = useState([]);
    const [autoComplete, setAutoComplete] = useState([]);
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(async () => {
        setLoading(true);
        if (city === '') {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(getLocation, posError);
            } else {
                alert('Geolocation Gagal');
            }
        }

        await Axios.get(`http://api.weatherapi.com/v1/forecast.json?key=a04d5b4b5c374d40a14135154221201&q=${city}&days=10&aqi=no&alerts=no`).then(res => {
            console.log(res.data);
            setDataCurrent(res.data)
            setDataForecast(res.data.forecast.forecastday);
        })
        setLoading(false);
    }, [city])

    const getLocation = (position) => {
        setCity(`${position.coords.latitude},${position.coords.longitude}`)
    }

    const posError = () => {
        alert('Get Location Error!')
    }

    useEffect(() => {
        if (search != null && search != '' && search != undefined) {
            Axios.get(`http://api.weatherapi.com/v1/search.json?key=a04d5b4b5c374d40a14135154221201&q=${search}`).then(res => {
                console.log(res.data);
                setAutoComplete(res.data)
            })
        } else {
            setAutoComplete([])
        }

    }, [search])

    const _searchAction = (param) => {
        setCity(param);
        setSearch('');
    }

    return (
        <div className='container'>
            <div className='card-weather'>
                <div className='card-current'>
                    <p className='text-title'>Today's Weather</p>

                    <img className='img-condition' src={dataCurrent?.current?.condition?.icon} />
                    <p className='text-condition'>{dataCurrent?.current?.condition?.text}</p>

                    <p className='text-celcius'>{dataCurrent?.current?.temp_c}°C</p>
                    <p className='text-location1'>{dataCurrent?.location?.name}</p>
                    <p className='text-location2'>{dataCurrent?.location?.region}, {dataCurrent?.location?.country}</p>
                    {loading ? <LinearProgress /> : null}
                </div>
                <div className='card-forecast'>
                    <div className='card-forecast-header'>
                        <p>Forecast</p>
                    </div>
                    <div className='card-forecast-line' />
                    <div className='card-forecast-body'>
                        {
                            dataForecast.map((res, index) => {
                                const day = moment(res?.date).format('dddd DD');
                                return (
                                    <div key={index} className='card-list-forecast'>
                                        <p className='text-list-forecast'>{day} </p>
                                        <img className='icon-forecast' src={res?.day?.condition?.icon} />
                                        <p className='text-list-forecast'>{res?.day?.mintemp_c}°C - {res?.day?.maxtemp_c}°C</p>
                                        <p className='text-list-forecast'>{res?.day?.condition?.text} </p>
                                        <p className='text-list-forecast'>{res?.day?.maxwind_mph} mph</p>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                <div className='card-search'>
                    <div className='input-search'>
                        <TextField
                            label="Search"
                            variant="standard"
                            style={{ width: '100%' }}
                            value={search}
                            onChange={(e) => setSearch(e.target.value)}
                        />
                    </div>
                    <div className='card-autocomplete'>
                        {
                            autoComplete.map((res, index) => {
                                return (
                                    <div key={index} className='card-list-autocomplete' onClick={() => _searchAction(res.name)}>
                                        <p>{res?.name}</p>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
